import pandas
from pandas.testing import assert_frame_equal
import pytest
from amazon_flatfile_generator.service import DataCollector


@pytest.fixture
def sample_parent_response():
    """ Minimal response containg the relevant parts """
    return {
        'id': 1234,
        'isMain': True,
        'itemId': 1010100001,
        'number': '1234',
        'properties': [
            {
                'propertyId': 1,
                'propertyRelation': {'cast': 'int'},
                'relationValues': [{'lang': 'DE', 'value': 12}]
            },
            {
                'propertyId': 2,
                'propertyRelation': {'cast': 'shortText'},
                'relationValues': [
                    {'lang': 'DE', 'value': "hose"},
                    {'lang': 'EN', 'value': "pants"}
                ]
            },
            {
                'propertyId': 3,
                'propertyRelation': {'cast': 'selection'},
                'relationValues': [{'lang': 0, 'value': 1}]
            }
        ]
    }


@pytest.fixture
def sample_child_response():
    """ Minimal response containg the relevant parts """
    return {
        'id': 1235,
        'isMain': False,
        'itemId': 1010100001,
        'number': '1235',
        'properties': [
            {
                'propertyId': 1,
                'propertyRelation': {'cast': 'int'},
                'relationValues': [{'lang': 'DE', 'value': 12}]
            },
            {
                'propertyId': 2,
                'propertyRelation': {'cast': 'shortText'},
                'relationValues': [
                    {'lang': 'DE', 'value': "hose"},
                    {'lang': 'EN', 'value': "pants"}
                ]
            },
            {
                'propertyId': 3,
                'propertyRelation': {'cast': 'selection'},
                'relationValues': [{'lang': 0, 'value': 1}]
            },
            {
                'propertyId': 4,
                'propertyRelation': {'cast': 'selection'},
                'relationValues': []
            }
        ],
        'images': {'0': 'test_url.com/1', '1': 'test_url.com/2'}
    }


@pytest.fixture
def sample_mapping():
    return {
        3: {1: {'de': 'abc'}}
    }


@pytest.fixture
def collector(monkeypatch):
    def fake_connect(self):
        return None
    monkeypatch.setattr(DataCollector, 'connect', fake_connect)
    return DataCollector(
        config={'columns': {'test': '-parent,number'}},
        data_folder='abc', json_paths={},
        alt_json_paths={}, lang='de'
    )


def describe_get_column_data():
    def with_child_when_parent_required(sample_child_response, sample_mapping,
                                        collector):
        json_path = {'test': ['-parent', 'number']}
        expected = ''
        result = collector.get_column_data(
            column='test', variation=sample_child_response,
            mapping=sample_mapping, json_path=json_path
        )
        assert expected == result

    def with_parent_when_parent_required(sample_parent_response,
                                         sample_mapping, collector):
        json_path = {'test': ['-parent', 'number']}
        expected = '1234'
        result = collector.get_column_data(
            column='test', variation=sample_parent_response,
            mapping=sample_mapping, json_path=json_path
        )
        assert expected == result

    def with_no_match(sample_child_response, sample_mapping, collector):
        json_path = {'test': ['properties', 'propertyId:99', 'feature']}
        expected = ''
        result = collector.get_column_data(
            column='test', variation=sample_child_response,
            mapping=sample_mapping, json_path=json_path
        )
        assert expected == result

    def with_integer_feature_match(sample_child_response, sample_mapping,
                                   collector):
        json_path = {'test': ['properties', 'propertyId:1', 'feature']}
        expected = 12
        result = collector.get_column_data(
            column='test', variation=sample_child_response,
            mapping=sample_mapping, json_path=json_path
        )
        assert expected == result

    def with_shorttext_feature_match(sample_child_response, sample_mapping,
                                     collector):
        json_path = {'test': ['properties', 'propertyId:2', 'feature']}
        expected = 'hose'
        result = collector.get_column_data(
            column='test', variation=sample_child_response,
            mapping=sample_mapping, json_path=json_path
        )
        assert expected == result

    def with_selection_feature_match(sample_child_response, sample_mapping,
                                     collector):
        json_path = {'test': ['properties', 'propertyId:3', 'feature']}
        expected = 'abc'
        result = collector.get_column_data(
            column='test', variation=sample_child_response,
            mapping=sample_mapping, json_path=json_path
        )
        assert expected == result

    def with_shorttext_with_different_lang(sample_child_response,
                                           sample_mapping,
                                           collector):
        json_path = {'test': ['properties', 'propertyId:2', 'feature']}
        expected = 'pants'
        collector.lang = 'en'
        result = collector.get_column_data(
            column='test', variation=sample_child_response,
            mapping=sample_mapping, json_path=json_path
        )
        assert expected == result

    def with_no_matching_key(sample_child_response, sample_mapping, collector):
        json_path = {'test': ['-child', 'images', '2']}
        expected = ''
        collector.json_paths = json_path
        result = collector.get_column_data(
            column='test', variation=sample_child_response,
            mapping=sample_mapping, json_path=json_path
        )
        assert expected == result

    def with_empty_feature(sample_child_response, sample_mapping, collector):
        json_path = {'test': ['properties', 'propertyId:4', 'feature']}
        expected = ''
        result = collector.get_column_data(
            column='test', variation=sample_child_response,
            mapping=sample_mapping, json_path=json_path
        )
        assert expected == result


def describe_sort_by_parent():
    def with_no_parent(collector):
        sorting_dict = {'12341': 3}
        dataframe = pandas.DataFrame(
            [
                ['12341', 'child', '1234']
            ], columns=['item_sku', 'parent_child', 'parent_sku']
        )
        expectation = pandas.DataFrame(
            [
                ['12341', 'child', '1234']
            ], columns=['item_sku', 'parent_child', 'parent_sku']
        )
        assert_frame_equal(
            collector.sort_by_parent(
                dataframe=dataframe, sorting_dict=sorting_dict
            ), expectation
        )

    def with_one_parent(collector):
        sorting_dict = {'12341': 1, '12342': 2, '12343': 3, '12344': 4}
        dataframe = pandas.DataFrame(
            [
                ['12341', 'child', '1234'],
                ['12342', 'child', '1234'],
                ['12344', 'child', '1234'],
                ['12343', 'child', '1234'],
                ['1234', 'parent', '']
            ], columns=['item_sku', 'parent_child', 'parent_sku']
        )
        expectation = pandas.DataFrame(
            [
                ['1234', 'parent', ''],
                ['12341', 'child', '1234'],
                ['12342', 'child', '1234'],
                ['12343', 'child', '1234'],
                ['12344', 'child', '1234']
            ], columns=['item_sku', 'parent_child', 'parent_sku']
        )
        assert_frame_equal(
            collector.sort_by_parent(
                dataframe=dataframe, sorting_dict=sorting_dict
            ), expectation
        )

    def with_two_parents(collector):
        sorting_dict = {
            '12341': 1, '12342': 2, '12343': 3, '12344': 4,
            '23451': 1, '23452': 2, '23453': 3
        }
        dataframe = pandas.DataFrame(
            [
                ['2345', 'parent', ''],
                ['12341', 'child', '1234'],
                ['23451', 'child', '2345'],
                ['12342', 'child', '1234'],
                ['23453', 'child', '2345'],
                ['12344', 'child', '1234'],
                ['23452', 'child', '2345'],
                ['12343', 'child', '1234'],
                ['1234', 'parent', '']
            ], columns=['item_sku', 'parent_child', 'parent_sku']
        )
        expectation = pandas.DataFrame(
            [
                ['1234', 'parent', ''],
                ['12341', 'child', '1234'],
                ['12342', 'child', '1234'],
                ['12343', 'child', '1234'],
                ['12344', 'child', '1234'],
                ['2345', 'parent', ''],
                ['23451', 'child', '2345'],
                ['23452', 'child', '2345'],
                ['23453', 'child', '2345']
            ], columns=['item_sku', 'parent_child', 'parent_sku']
        )
        assert_frame_equal(
            collector.sort_by_parent(
                dataframe=dataframe, sorting_dict=sorting_dict
            ), expectation
        )

    def with_duplicate_sorting_numbers(collector):
        sorting_dict = {'12341': 1, '12342': 2, '12343': 2, '12344': 3}
        dataframe = pandas.DataFrame(
            [
                ['12341', 'child', '1234'],
                ['12342', 'child', '1234'],
                ['12344', 'child', '1234'],
                ['12343', 'child', '1234'],
                ['1234', 'parent', '']
            ], columns=['item_sku', 'parent_child', 'parent_sku']
        )
        expectation = pandas.DataFrame(
            [
                ['1234', 'parent', ''],
                ['12341', 'child', '1234'],
                ['12342', 'child', '1234'],
                ['12343', 'child', '1234'],
                ['12344', 'child', '1234']
            ], columns=['item_sku', 'parent_child', 'parent_sku']
        )
        assert_frame_equal(
            collector.sort_by_parent(
                dataframe=dataframe, sorting_dict=sorting_dict
            ), expectation
        )

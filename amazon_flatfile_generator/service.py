"""
Amazon Flatfile Generator - Generate a Amazon Flatfile from the Plentymarkets
REST API.
Copyright (C) 2022  Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from configparser import ConfigParser
from typing import Any, Dict, List, Tuple, Union
from openpyxl.workbook.workbook import Workbook
import plenty_api
import openpyxl as xl
from openpyxl.utils.dataframe import dataframe_to_rows
import pathlib
import pandas

from loguru import logger


class DataCollector:
    def __init__(
        self, config, data_folder: pathlib.Path,
        json_paths: Dict[str, List[str]], alt_json_paths: Dict[str, List[str]],
        lang: str, referrer_id: float = None, debug: bool = False
    ) -> None:
        self.config = config
        self.data_folder = data_folder
        self.json_paths = json_paths
        self.alt_json_paths = alt_json_paths
        self.lang = lang
        self.referrer_id = referrer_id
        self.api = self.connect(debug=debug)
        self.plenty_price_config = self.get_plenty_price_config_data()

    def connect(self, debug: bool) -> plenty_api.PlentyApi:
        """Connect to the plentyAPI"""
        return plenty_api.PlentyApi(base_url=self.config['plenty']['base_url'],
                                    debug=debug)

    def get_selection_mapping(self) -> Dict[str, Dict[str, Dict[str, str]]]:
        """
        Fetch a mapping of variation-properties (features) to corresponding
        selections with values for all available languages.

        Return:
                            [dict]      -   Example:
                {prop_id: {selection_id: {'de': 'value_de', 'en': 'value_en'}}}
        """
        selection_mapping = self.api.plenty_api_get_property_selections()

        if selection_mapping == {}:
            logger.error("Selection mapping pull request failed.")
            return {}

        return dict(selection_mapping)

    def get_variations(self, refine: dict) -> List[Dict[str, Any]]:
        """
        Pull all variations matching the filter, fetch all required additional
        data for the flatfile.

        Parameters:
            refine          [dict]      -   Can either be empty
                                            (pull all variations),
                                            {'id': ..}
                                            (pull a single variation),
                                            {'itemId': ..}
                                            (pull all variations from the item)

        Return:
                            [list]      -   list of JSON responses from the
                                            REST API containing the variations
        """
        entries = self.api.plenty_api_get_variations(
            refine=refine, additional=[
                'variationBarcodes', 'variationDefaultCategory', 'images',
                'variationSalesPrices', 'variationAttributeValues', 'parent',
                'item', 'properties', 'variationSkus'
            ]
        )

        if not entries:
            logger.error("Variation pull request failed.")
            return []

        return entries

    def read_template(
        self
    ) -> Tuple[Union[List[str], None], Union[Workbook, None]]:
        """
        Take an empty Amazon flatfile as blueprint for the output file.

        The file is used to make sure that the resulting output matches is
        up-to-date.

        Result:
                            [tuple]     -   A list of the used columns and
                                            workbook data-structure of the
                                            spreadsheet created by openpyxl.
        """
        path = self.data_folder / self.config['general']['template_file']
        if not path.exists():
            logger.error(f"File at path: {path} does not exist.")
            return (None, None)

        sheet = self.config['general']['template_sheet']
        workbook = xl.load_workbook(filename=path)
        try:
            sheet_data = workbook[sheet].values
        except KeyError:
            logger.error(f"Sheet {sheet} at path {path} does not exist.")
            return (None, None)

        sheet_data = list(sheet_data)
        columns = [x for x in sheet_data[2] if x is not None]

        return (columns, workbook)

    def get_column_data(
        self, column: str, variation: Dict[str, Any],
        mapping: Dict[str, Dict[str, Dict[str, str]]],
        json_path: Dict[str, List[str]]
    ) -> str:
        if column not in self.config['columns'].keys():
            return ''

        data = variation
        for key in json_path[column]:
            if key == '-child':
                if variation['isMain']:
                    return ''
                continue
            if key == '-parent':
                if not variation['isMain']:
                    return ''
                continue
            if key == '-default':
                # The last part of the config option value must be the
                # default value
                return json_path[column][-1]

            if isinstance(data, list):
                if not data:
                    return ''
                key_value_pair = key.split(':')
                for entry in data:
                    if entry[key_value_pair[0]] == int(key_value_pair[1]):
                        data = entry
                if isinstance(data, list):
                    return ''
            else:
                if key == 'feature':
                    cast = data['propertyRelation']['cast']
                    entries = data['relationValues']
                    if not entries:
                        return ''
                    if cast == 'selection':
                        selection_value_map = mapping[data['propertyId']]
                        selection_id = int(entries[0]['value'])
                        try:
                            data = selection_value_map[selection_id][self.lang]
                        except KeyError:
                            logger.warning(
                                "No selection translation found for language "
                                f"{self.lang} at property ID {data['propertyId']} "
                                f"selection ID {selection_id} "
                                f"of variation {variation['id']}"
                            )
                            return ''
                    elif cast in ['shortText', 'longText']:
                        for entry in entries:
                            if entry['lang'].lower() == self.lang.lower():
                                data = entry['value']
                    elif cast in ['int', 'float', 'date', 'file']:
                        try:
                            data = entries[0]['value']
                        except IndexError:
                            logger.warning(
                                "No value found for feature ID "
                                f"{data['propertyId']} of variation "
                                f"{variation['id']}"
                            )
                    if isinstance(data, dict):
                        return ''
                    break
                try:
                    data = data[key]
                except KeyError:
                    return ''
                if key == 'amazonProductType':
                    try:
                        data = self.product_type_mapping[data]
                    except KeyError:
                        logger.warning(
                            "Invalid Amazon product type at "
                            f"variation {variation['id']}"
                        )
                        return ''
        return data

    def get_images(self, variation: Dict[str, Any]) -> None:
        images = variation['images']
        image_dict = {}
        for image in images:
            if self.referrer_id:
                for availability in image['availabilities']:
                    if availability['value'] == self.referrer_id:
                        image_dict[image['position']] = image['url']
                        break
            else:
                image_dict[image['position']] = image['url']
        image_list_sorted = sorted(list(image_dict.keys()))
        image_dict_sorted = {}
        for index, position in enumerate(image_list_sorted):
            image_dict_sorted[str(index)] = image_dict[position]
        variation['images'] = image_dict_sorted

    def get_attributes(
        self, variation: Dict[str, Any], attributes: List[Dict[str, Any]]
    ) -> None:
        attribute_values = variation['variationAttributeValues']
        for atrribute in attributes:
            for attribute_value in attribute_values:
                if attribute_value['attributeId'] == atrribute['id']:
                    for value in atrribute['values']:
                        if value['id'] == attribute_value['valueId']:
                            for name in value['valueNames']:
                                if name['lang'] == self.lang.lower():
                                    attribute_value['value'] = name['name']


    def _build_amazon_product_types_legacy_mapping(self, amazon_product_types: List[dict]) -> Dict[int, str]:
        """Create the legacy mapping originally created from a csv file,
        from the plenty_api pim amazon product types request response.
        pairs amazon_product_type_id (key) with amazon_product_type_name (value)
        """
        return {product_type['id']: product_type['type'] for product_type in amazon_product_types}

    def get_product_type_mapping(self) -> Dict[int, str]:
        """ Get the mapping of Amazon product types from the plenty_api pim amazon product types request response.
        mapps amazon_product_type_id (key) with amazon_product_type_name (value)
        """
        amazon_product_types = self.api.plenty_api_get_amazon_product_types()
        return self._build_amazon_product_types_legacy_mapping(amazon_product_types)

    def get_sorting_numbers(
        self, variations: List[Dict[str, Any]]
    ) -> Dict[str, int]:
        return {
            variation['number']: variation['position']
            for variation in variations
            if not variation['isMain']
        }

    def build_dataframe(
        self, variations: List[Dict[str, Any]], columns: List[str],
        mapping: Dict[str, Dict[str, Dict[str, str]]]
    ) -> pandas.DataFrame:
        data = []
        self.parent_dict = {}
        attributes = self.api.plenty_api_get_attributes(additional=['values'])
        self.product_type_mapping = self.get_product_type_mapping()

        for variation in variations:
            self.get_attributes(variation, attributes)
            if variation['isMain']:
                self.parent_dict[variation['number']] = variation['itemId']
            self.get_images(variation=variation)
            row = []

            for column in columns:

                if column == 'parent_child':
                    if variation['isMain']:
                        row.append('parent')
                    else:
                        row.append('child')
                    continue
                if column == 'parent_sku':
                    if variation['isMain']:
                        row.append('')
                        continue
                    skus = variation['variationSkus']
                    parent_sku = ''
                    for sku in skus:
                        main_amazon_account = sku['accountId'] == 0
                        amazon_market_sku = sku['marketId'] in [4, 104]
                        if main_amazon_account and amazon_market_sku:
                            parent_sku = sku['parentSku']
                            break
                    if not parent_sku:
                        logger.warning(f"Variation {variation['id']} does not "
                                       "have a configured Amazon SKU. Unable "
                                       "to fetch a valid parent SKU.")
                        row.append('')
                    else:
                        row.append(parent_sku)
                    continue

                if column in ['standard_price', 'business_price', 'list_price_with_tax']:
                    if variation['isMain']:
                        row.append('')
                        continue
                    if column == 'list_price_with_tax':
                        row.append(self._get_variation_price(variation, 'list'))
                    else:
                        row.append(self._get_variation_price(variation, 'sales'))
                    continue

                if column not in self.json_paths.keys():
                    row.append('')
                    continue
                row.append(
                    self.get_column_data(
                        column=column, variation=variation, mapping=mapping,
                        json_path=self.json_paths
                    )
                )
                if row[-1] == '' and column in self.alt_json_paths.keys():
                    row[-1] = self.get_column_data(
                        column=column, variation=variation, mapping=mapping,
                        json_path=self.alt_json_paths
                    )
            data.append(row)

        dataframe = pandas.DataFrame(data, columns=columns)

        return dataframe

    def _get_variation_price(self, variation: Dict[str, Any],
                             target: str) -> float:
        """Get the prices for a variation from the plenty_api pim request response.
        """
        if target not in ['sales', 'list']:
            raise ValueError("Invalid target, choose 'sales' or 'list'")

        variation_sales_prices_data = variation['variationSalesPrices']
        config_price_id = self.__get_config_language_price_id(target)
        price_value = self.__get_price_value(config_price_id, variation_sales_prices_data)

        return price_value

    def __get_config_language_price_id(self, target: str) -> int:
        """Get the sales price id from the config file for the set language.
        via the language_price_mapping.
        """
        if target == 'sales':
            key = 'languages_price_map'
        else:
            key = 'languages_list_price_map'

        try:
            return int(self.config[key][self.lang])
        except KeyError as err:
            raise KeyError("Incomplete configuration, missing mapping of "
                           f"languages to plentymarkets {target} price IDs")

    def __get_price_value(self, price_id: int, variation_sales_prices: list) -> float:
        """fetch/filter sales price for the configured sales price id from list of sales prices dicts"""
        for price_data in variation_sales_prices:
            if price_data['salesPriceId'] == price_id:
                return price_data['price']

        raise ValueError(f"Price id {price_id} not found in variation")

    def insert_product_description(self, dataframe: pandas.DataFrame) -> None:
        items = self.api.plenty_api_get_items(lang=self.lang.lower())
        if not items:
            raise RuntimeError('Item pull request failed')
        for index, row in dataframe.iterrows():
            for item in items:
                try:
                    if self.parent_dict[row['parent_sku']] == item['id']:
                        for text in item['texts']:
                            if text['lang'] == self.lang.lower():
                                dataframe.loc[
                                    index, 'product_description'
                                ] = text['description']
                except KeyError:
                    try:
                        if self.parent_dict[row['item_sku']] == item['id']:
                            for text in item['texts']:
                                if text['lang'] == self.lang.lower():
                                    dataframe.loc[
                                        index, 'product_description'
                                    ] = text['description']
                    except KeyError:
                        continue #! TODO: NEEDS SOLID ERROR HANDLING

    def sort_by_parent(
        self, dataframe: pandas.DataFrame, sorting_dict: Dict[str, int]
    ) -> pandas.DataFrame:
        assert all(
            x in dataframe.columns
            for x in ['item_sku', 'parent_child', 'parent_sku']
        )
        result_table = pandas.DataFrame(columns=dataframe.columns)
        parents = dataframe[dataframe['parent_child'] == 'parent']['item_sku']
        parents = parents.sort_values()

        if len(parents.index) == 0:
            return dataframe

        for parent in parents.to_list():
            children = dataframe.loc[dataframe['parent_sku'] == parent].copy() #! COULD ADD WARNING IF CHILDREN GET DROPED.?

            children['sort_number'] = children['item_sku'].apply(
                lambda x: sorting_dict[x]
            )
            children.sort_values(by=['sort_number', 'item_sku'], inplace=True)
            children.drop('sort_number', axis=1, inplace=True)
            parent_row = dataframe[dataframe['item_sku'] == parent]

            result_table = result_table.append(parent_row)
            result_table = result_table.append(children)

        result_table.reset_index(drop=True, inplace=True)
        return result_table


    def get_plenty_price_config_data(self):
        """Get information about the price ids and currencies from the plenty_api.
        _quick and dirty solution to get the currency symbol for the price ids.
        returns a dict with price id as key and currency and name as values.
        """
        plenty_price_config = self.api.plenty_api_get_price_configuration()
        assert plenty_price_config, 'Price configuration request failed'
        return {
            price_config['id']: self.__get_price_config_values(price_config)
            for price_config in plenty_price_config
            }

    def __get_price_config_values(self, price_config: dict):
        """Get the currency and price id from the price config dict.
        Helper To get nested price config values for price config values.
        """
        price_id = price_config['id']
        currency = price_config['currencies'][0].get('currency')
        name = price_config['names'][0].get('nameInternal')
        return {
                'currency': currency,
                'salesPriceId': price_id,
                'name': name
                }


class Writer:
    def __init__(
        self, dataframe: pandas.DataFrame, workbook: Workbook,
        config: ConfigParser
    ) -> None:
        self.dataframe = dataframe
        self.workbook = workbook
        self.config = config

    def write_to_workbook(self, file_path: pathlib.Path) -> None:
        worksheet = self.workbook[self.config['general']['template_sheet']]
        for row in dataframe_to_rows(
            self.dataframe, index=False, header=False
        ):
            worksheet.append(row)
        self.workbook.save(file_path)

    def write_to_csv(self, file_path: pathlib.Path) -> None:
        self.dataframe.to_csv(file_path, sep=';', index=False)

"""
Amazon Flatfile Generator - Generate a Amazon Flatfile from the Plentymarkets
REST API.
Copyright (C) 2022  Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse
import configparser
import os
import pathlib
import sys
from typing import Dict, List
import platformdirs

from loguru import logger

from amazon_flatfile_generator.service import DataCollector
from amazon_flatfile_generator.interface import Interface


def parse_config_columns(
    config: configparser.ConfigParser, target: str = 'columns'
) -> Dict[str, List[str]]:
    if target == 'columns' and not config.has_section(target):
        logger.error("Please configure your flatfile columns section "
                     f"[{target}]")
        sys.exit(1)
    elif target == 'alternatives' and not config.has_section(target):
        logger.warning("No alternatives configured.")
        return {}
    json_paths = {}
    for column, json_path in config[target].items():
        json_paths[column] = json_path.split(',')

    return json_paths


def get_paths(args: argparse.Namespace) -> Dict[str, pathlib.Path]:
    """
    Get the locations for the configuration, the required input data and the
    destination for output files.

    Priority: CLI arguments > Environment variables > Default

    Returns:
                            [dict]      -   Dictionary of locations for the 3
                                            different purposes
    """
    documents = pathlib.Path(platformdirs.user_documents_dir())
    paths = {
        'config': pathlib.Path(
            platformdirs.user_config_dir('amazon_flatfile_generator')
        ),
        'data': documents / 'amazon_flatfile_data',
        'output': documents
    }

    for location in paths:
        env_string = f'AMAZON_FLATFILE_GENERATOR_{location.upper()}_DIR'
        env = os.environ.get(env_string)
        arg = getattr(args, location)
        if env:
            paths[location] = pathlib.Path(env)
            if args.debug and not arg:
                logger.debug(
                    f"Use {location} location at {paths[location]} from the "
                    f"environment variable {env_string}"
                )
        if arg:
            paths[location] = arg
            if args.debug:
                logger.debug(
                    f"Use {location} location at {paths[location]} from the "
                    f"command line argument {location}"
                )
        if args.debug and not arg and not env:
            logger.debug(
                f"Use default {location} location at {paths[location]}"
            )

    return paths


def read_config(path: pathlib.Path) -> configparser.ConfigParser:
    if not path.exists():
        path.mkdir(parents=True)
    config_path = path / 'config.ini'
    config_path.touch(exist_ok=True)
    config = configparser.ConfigParser()
    config.read(config_path)
    return config


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--custom_config_dir', '--config', '-c', dest='config',
        type=pathlib.Path,
        help='Specify a custom location for the configuration'
    )
    parser.add_argument(
        '--custom_data_dir', '--data', '-a', dest='data', type=pathlib.Path,
        help='Specify a custom location for the data'
    )
    parser.add_argument(
        '--custom_output_dir', '--output', '-o', dest='output',
        type=pathlib.Path,
        help='Specify a custom location for the output files'
    )
    parser.add_argument(
        '--debug', '-d', dest='debug', action='store_true',
        help='Provide more output for error diagnostic'
    )
    args = parser.parse_args()
    paths = get_paths(args=args)
    config = read_config(path=paths['config'])
    json_paths = parse_config_columns(config=config)
    alt_json_paths = parse_config_columns(config=config, target='alternatives')

    defaults = {
        'language': config.get(section='general', option='default_language',
                               fallback=''),
        'origin': config.get(section='general', option='default_origin',
                             fallback=''),
        'file_format': config.get(section='general',
                                  option='default_file_format', fallback=''),
    }
    if defaults['language']:
        default_language = config['languages'][defaults['language']]
    else:
        default_language = 'de'
    data_collector = DataCollector(
        config=config, data_folder=paths['data'], json_paths=json_paths,
        alt_json_paths=alt_json_paths,
        lang=default_language, debug=args.debug)
    interface = Interface(config=config, data_collector=data_collector,
                          defaults=defaults, output_dir=paths['output'])
    interface.root.mainloop()

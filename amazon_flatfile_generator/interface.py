"""
Amazon Flatfile Generator - Generate a Amazon Flatfile from the Plentymarkets
REST API.
Copyright (C) 2022  Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import tkinter as tk
import pathlib
import re

from tkinter import messagebox
from tkinter import ttk
from typing import Any, Dict, Tuple, Union

from amazon_flatfile_generator.service import DataCollector, Writer


def OptionMenu_SelectionEvent(event):
    # print(var.get())
    print("variable changed! EVENT OPTION")

def callback(*args):
    print("variable changed!")

class Interface:
    def __init__(
        self, config, data_collector: DataCollector, defaults: Dict[str, Any],
        output_dir: pathlib.Path
    ) -> None:
        self.config = config
        self.data_collector = data_collector
        self.defaults = defaults
        self.output_dir = output_dir
        self.root = tk.Tk()
        self.root.wm_title("Amazon flatfiles")
        self.build_interface()

    def check_for_input_error(
        self
    ) -> Tuple[Union[pathlib.Path, None], Union[Dict[str, Any], None]]:
        if not self.name.get():
            messagebox.showerror("No filename chosen", "Please choose a name "
                                 "for your flatfile.")
            return (None, None)

        if re.search(r"\.", self.name.get()):
            messagebox.showerror("File-extension detected", "Please omit all "
                                 "file-extensions when naming your file")
            return (None, None)

        filename = self.name.get()
        if self.file_format.get() == "Spreadsheet":
            filename = self.name.get() + ".xlsx"
        elif self.file_format.get() == "CSV":
            filename = self.name.get() + ".csv"
        else:
            messagebox.showerror("No Output-format selected", "Please select a"
                                 " format")
            return (None, None)

        file_path = self.output_dir / filename

        if not self.language.get():
            messagebox.showerror("No language selected", "Please select a "
                                 "language.")
            return (None, None)
        self.data_collector.lang = self.config['languages'][self.language.get()]

        if self.item.get() and self.variation.get():
            messagebox.showerror("Multiple filters selected",
                                 "You can only filter according to one "
                                 "criterion.")
            return (None, None)

        refine = None
        if self.item.get():
            refine = {"itemId": self.item.get()}
        if self.variation.get():
            refine = {"id": self.variation.get()}
        if self.origin.get():
            try:
                referrer_id = float(self.config['origins'][self.origin.get()])
            except ValueError:
                messagebox.showerror(
                    "Invalid value format for origin Id",
                    "Value format for origin IDs must be floating point "
                    "numbers.")
                return (None, None)
            if refine:
                refine['referrerId'] = referrer_id
            else:
                refine = {'referrerId': referrer_id}
            self.data_collector.referrer_id = referrer_id

        return (file_path, refine)

    def __next_step(self, text: str, value: int) -> None:
        """
        Jump to the next step of the application while updating the GUI.

        Parameters:
            text            [str]       -   New status text for the GUI
            value           [int]       -   Progressbar percentage value
        """
        self.progress_bar['value'] = value
        self.status['text'] = text
        self.root.update()

    def create_flatfile(self) -> None:
        self.__next_step(text="Checking user input", value=0)
        (file_path, refine) = self.check_for_input_error()

        if not file_path:
            self.status['text'] = "User input error"
            return

        self.__next_step(text="Pulling variation data from PlentyMarkets",
                         value=14)
        variations = self.data_collector.get_variations(refine=refine)
        if not variations:
            self.status['text'] = "Variation pull failed"
            return

        self.__next_step(text="Reading flatfile template", value=28)
        (columns, workbook) = self.data_collector.read_template()
        if not columns or not workbook:
            self.status['text'] = "Reading flatfile template failed"
            return

        self.__next_step(text="Pulling property selection values", value=42)
        mapping = self.data_collector.get_selection_mapping()
        if not mapping:
            self.status['text'] = "Pulling property selection values failed"
            return

        self.__next_step(text="Building flatfile", value=56)
        dataframe = self.data_collector.build_dataframe(
            variations=variations, columns=columns, mapping=mapping
        )
        sorting_dict = self.data_collector.get_sorting_numbers(
            variations=variations
        )

        self.__next_step(text="Inserting product descriptions", value=70)
        self.data_collector.insert_product_description(dataframe=dataframe)
        dataframe = self.data_collector.sort_by_parent(
            dataframe=dataframe, sorting_dict=sorting_dict
        )

        writer = Writer(dataframe=dataframe, workbook=workbook,
                        config=self.config)

        self.__next_step(text="Writing data into file", value=84)
        if self.file_format.get() == "CSV":
            writer.write_to_csv(file_path=file_path)
        if self.file_format.get() == "Spreadsheet":
            writer.write_to_workbook(file_path=file_path)

        self.__next_step(text="Execution complete", value=100)
        self.task_finished_message(file_path)

    def _sales_price_info(self):
        """Get Plenty SalesPrice Data Info for set languange (Currency and Name)"""
        lang = self.config['languages'][self.language.get()]
        sales_prcie_id = int(self.config['languages_price_map'][lang])
        sales_price_info = self.data_collector.plenty_price_config[sales_prcie_id]
        return sales_price_info

    def language_option_selection_event(self, event):
        """EVENT: Change Info_bar Text on language change - for Salesprice and Currency."""
        
        sales_price_info = self._sales_price_info()
        msg_text = f'Language set to: {event.upper()} with salesPriceId {sales_price_info["salesPriceId"]}: \
                    Flatfile currency:  {sales_price_info["currency"]}'
        
        self.info_bar.grid(row=6, column=0, columnspan=6, sticky="wens")
        self.info_bar_text.set(msg_text)
        self.info_bar.config(fg='#F6A623')
        return print(f"language_option Changed! {event}: {msg_text}")
    
    def task_finished_message(self, file_path):
        """
        and also print message to console for user to see.         
        
        this  can display a "silent" messagebox not popping up in the foreground.
        """
        sales_price_info = self._sales_price_info()
       
        msg_text =  f"The Flatfile creation completed \
                    \n\n Filename: {self.name.get()} \
                    \n\n Flatfile Currency: {sales_price_info['currency']} \
                    \n\n File Saved to: {file_path}"
        
        print('\033[36m' + '\033[1m' + msg_text + '\033[0m') # DARKCYAN + BOLD + RESET
        
        # DISABLED: on request of the user, the messagebox is not displayed for now anymore.
        # return messagebox.showinfo( 
        #                 title="Flatfile Task finished", 
        #                 message=msg_text, 
        #                 icon=None, 
        #                 type=None)
 



    def build_interface(self) -> None:
        l_1 = tk.Label(self.root, text="Create amazon flatfiles")
        l_2 = tk.Label(self.root, text="Item Id (filter option):")
        l_3 = tk.Label(self.root, text="Variation Id (filter option):")
        l_4 = tk.Label(self.root, text="Name of the template file:")
        l_5 = tk.Label(self.root, text="Output-format:")
        l_6 = tk.Label(self.root, text="Language:")
        l_7 = tk.Label(self.root, text="Origin Id (filter option):")
        self.status = tk.Label(self.root, text="Ready for execution.")
        self.info_bar_text = tk.StringVar(value="Info")
        self.info_bar = tk.Label(self.root, text="Info", textvariable=self.info_bar_text)
        self.progress_bar = ttk.Progressbar(self.root, orient='horizontal',
                                            mode='determinate', length=400)
        self.item = tk.Entry(self.root)
        self.variation = tk.Entry(self.root)
        self.name = tk.Entry(self.root)
        formats = ['Spreadsheet', 'CSV']
        self.file_format = tk.StringVar()
        format_menu = tk.OptionMenu(self.root, self.file_format, *formats)
        languages = self.config['languages'].keys()
        self.language = tk.StringVar()
        self.language.trace("w", callback)
        language_menu = tk.OptionMenu(self.root, self.language, *languages, command = self.language_option_selection_event)
        origins = self.config['origins'].keys()
        self.origin = tk.StringVar()
        origin_menu = tk.OptionMenu(self.root, self.origin, *origins)
        run_button = tk.Button(self.root, text="Create flatfile",
                               command=lambda: self.create_flatfile())
        for default in self.defaults:
            if self.defaults[default]:
                getattr(self, default).set(self.defaults[default])

        l_1.grid(row=0, column=0, columnspan=6, sticky="wens")
        l_4.grid(row=1, column=0, sticky="wens")
        self.name.grid(row=1, column=1, sticky="wens")
        l_3.grid(row=1, column=2, sticky="wens")
        self.variation.grid(row=1, column=3, sticky="wens")
        l_2.grid(row=1, column=4, sticky="wens")
        self.item.grid(row=1, column=5, sticky="wens")
        l_6.grid(row=2, column=0, sticky="wens")
        language_menu.grid(row=2, column=1, sticky="wens")
        l_5.grid(row=2, column=2, sticky="wens")
        format_menu.grid(row=2, column=3, sticky="wens")
        l_7.grid(row=2, column=4, sticky="wens")
        origin_menu.grid(row=2, column=5, sticky="wens")
        run_button.grid(row=3, column=0, columnspan=6, sticky="wens")
        self.progress_bar.grid(row=4, column=0, columnspan=6, sticky="wens")
        self.status.grid(row=5, column=0, columnspan=6, sticky="wens")
        # self.info_bar.grid(row=6, column=0, columnspan=6, sticky="wens")


